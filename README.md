# Terminal Laravel



## Câu lệnh tải Laravel 8 thông qua Composer :

```bash
    composer create-project --prefer-dist laravel/laravel folderName '8.*'
```

## Câu lệnh chạy server ảo trong Laravel :

```bash
    php artisan serve
```

## Câu lệnh để xem toàn bộ routes đang có trong PHP Framework Laravel :

```bash
    php artisan route:list
```

## Câu lệnh tạo controller trong Laravel :

```bash
    php artisan make:controller UserController
```

## Câu lệnh tạo controller với API trong Laravel :

```bash
    php artisan make:controller Api/ProductController --api
```

## Câu lệnh tạo controller với 7 functions CRUD trong Laravel :

```bash
    php artisan make:controller UserController --resource
```

## Câu lệnh tạo resource trong Laravel:

```bash
    php artisan make:resource UserResource
```

# Câu lệnh tạo Migration trong Laravel:

```bash
    php artisan make:migration create_users_table
```

## Câu lệnh chạy Migration:

```bash
    php artisan migrate
```

## Câu lệnh quay trở lại dựa trên dữ liệu đã ghi vào migrations table và chạy lại migration:

```bash
    php artisan migrate:refresh
```

## Câu lệnh xóa hết các bảng, không quan tâm về rollback và chạy lại migration:

```bash
    php artisan migrate:fresh
```

## Câu lệnh tạo file Seeder trong Laravel:

```bash
    php artisan make:seeder UsersTableSeeder
```

## Câu lệnh chạy file Seeder cụ thể với tên class trong Laravel:

```bash
    php artisan db:seed --class=UsersTableSeeder
```

## Câu lệnh chạy file DatabaseSeeder, có thể gọi tới nhiều file với class seeder cụ thể:

```bash
    php artisan db:seed
```

## Câu lệnh để xóa tất cả các bảng dữ liệu, sau đó chạy lại migration sau đó chạy file DatabaseSeeder, có thể gọi tới nhiều class seeder:

```bash
    php artisan migrate:fresh --seed
```

## Câu lệnh tạo Model trong PHP Framework Laravel :

```bash
    php artisan make:model User
```

## Câu lệnh kết hợp tạo Model và Controller trong Laravel :

```bash
    php artisan make:model User -c
```

## Câu lệnh kết hợp tạo Model, Controller và Migration trong PHP Framework Laravel:

```bash
    php artisan make:model Category -mc
```

## Câu lệnh kết hợp tạo Model, Controller + 7 functions CRUD và Migration trong PHP Framework Laravel:

```bash
    php artisan make:model User -mcr
```

## Câu lệnh tạo Factory trong PHP Framework Laravel :

```bash
    php artisan make:factory UserFactory
```

## Câu lệnh tạo Factory kết hợp thuộc tính model trong PHP Framework Laravel :

```bash
    php artisan make:factory UserFactory --model=User
```

## Câu lệnh tạo Livewire trong PHP Framework Laravel :

```bash
    php artisan make:livewire Admin/BrandComponent
```

## Câu lệnh tạo FormRequest trong PHP Framework Laravel :

```bash
    php artisan make:request User/UserFormRequest
```

## Câu lệnh tạo Feature Test trong PHP Framework Laravel :

```bash
    php artisan make:test User/UpdateUserTest
```

## Câu lệnh kiểm tra Feature Test Function trong PHP Framework Laravel :

1. Cách thứ nhất

```bash
    phpunit --filter functionName
```

2. Cách thứ hai

```bash
    php artisan test --filter functionName
```

3. Cách thứ ba: kiểm tra toàn bộ

```bash
    php artisan test
```

## Tham khảo thêm cách tạo terminal Laravel

1. Phần 1
[Link tham khảo](https://viblo.asia/p/danh-sach-cau-lenh-laravel-artisan-cung-voi-options-cua-chung-phan-1-RQqKLb7zl7z)

2. Phần 2
[Link tham khảo](https://viblo.asia/p/danh-sach-cau-lenh-laravel-artisan-cung-voi-options-cua-chung-phan-2-Az45bPA6ZxY)
